#!/usr/bin/python
# -*- coding: utf-8 -*-
from array import array
from asyncio.windows_events import NULL
import psycopg2

def work_on_file_dataset():
    file = open(r'C:\Users\neoro\Downloads\ODBC\src\Datensatz-Combined.txt', encoding="utf8")
    file_c = file.readlines()
    values = []
    for line in file_c:
        tmp = line.split(",", 6)
        if tmp[0] == '':
            continue
        if tmp[1] == '' :
            tmp[1] = 'keine Angaben'

        if tmp[2] == '':
            tmp[2] = 'keine Angaben'

        if tmp[3] == '':
            tmp[3] = 'keine Angaben'

        if tmp[4] == '':
            tmp[4] = NULL

        if tmp[5] == '':
            tmp[5] = 'keine Angaben'
        
        if tmp[6] == '\n':
            tmp[6] = '{-1}'
        else: 
            tmp[6] = '{' + tmp[6] + '}'

        values.append(tmp)
    file.close()
    return values

def work_on_file_hmd_tabelle():
    file = open(r'C:\Users\neoro\Downloads\ODBC\src\HMD_Tabelle.txt', encoding="utf8")
    file_c = file.readlines()
    values = []
    for line in file_c:
        tmp = line.split(",")

        values.append(tmp)
    file.close()
    return values

def work_on_file_nutzt_tabelle():
    file = open(r'C:\Users\neoro\Downloads\ODBC\src\NUTZT_Tabelle.txt', encoding="utf8")
    file_c = file.readlines()
    values = []
    for line in file_c:
        tmp = line.split(",", 1)
        if tmp[0] == '':
            continue

        if tmp[1] == '\n':
            continue
        else: 
            tmp[1] = tmp[1].split(",")
            tmp[1] = [i.replace("\n", "") for i in tmp[1]]
            for i in tmp[1]:
                helper = []
                helper.append(tmp[0])
                helper.append(i)
                values.append(helper)
                print(helper)

    file.close()
    return values

def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host="localhost", database="vrpaper", user="postgres", password="robert123") # man kann sich leider nicht mit MySql datenbanken verbinden, da es ja Postgre ist

        # create a cursor
        cur = conn.cursor()
        query1 = "INSERT INTO vrpaper.hmd (hmd_id, hmd_name) VALUES (%s, %s)"
        query2 = "INSERT INTO vrpaper.nutzt (doi, hmd_id) VALUES (%s, %s) ON CONFLICT(doi, hmd_id) DO NOTHING"
        query3 = "INSERT INTO vrpaper.paper (doi, autor, titel, sourcetitel, jahr, feld, hmd_id) VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT(doi) DO NOTHING"

        hmd_value = work_on_file_hmd_tabelle()
        nutzt_value = work_on_file_nutzt_tabelle()
        values = work_on_file_dataset()

        cur.executemany(query1, hmd_value)
        cur.executemany(query2, nutzt_value)
        cur.executemany(query3, values)

        conn.commit()
        
        print("SELECT doi, hmd_id FROM vrpaper.nutzt WHERE doi = 10.1364/OE.27.024877 LIMIT 20;")
        cur.execute("SELECT doi, hmd_id FROM vrpaper.nutzt WHERE doi = '10.1364/OE.27.024877' LIMIT 20") # hier der eigentliche Zugriff auf die Datenbank mit Abfrage

        # display the PostgreSQL database server version
        db_select = cur.fetchall()
        for r in db_select:
            print(f'doi: {r[0]} | hmd_id: {r[1]}')
            print('-------------------------------------------------------------------------------------------------')

        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            # hier kommt das SELECT * ... hin
            conn.close()
            print('Database connection closed.')


def main():
    connect()

if __name__ == '__main__':
    main()
